<?php

namespace App;

use App\Participante;
use Illuminate\Database\Eloquent\Model;

class Modalidad extends Model
{
    protected $table = 'modalidades';


    public function participantes(){
		return $this->hasMany('App\Participante');
	}

}
