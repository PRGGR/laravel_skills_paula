<?php

namespace App\Http\Controllers;

use App\Modalidad;
use Illuminate\Http\Request;

class ModalidadesController extends Controller
{
    public function getInicio(){
    	return redirect()->action('ModalidadesController@getModalidades');
    }

    public function getModalidades(){
		$modalidades = Modalidad::all();
	    return view('modalidades.index', 
	    	array('arrayModalidades' => $modalidades));
	}

	public function getModalidad($slug_modalidad){
		$modalidad = Modalidad::where('slug',$slug_modalidad)->first();
		return view('modalidades.mostrar',
				array('modalidad' => $modalidad));
	}

	public function postPuntuar($slug_modalidad){
		$modalidad = Modalidad::where('slug',$slug_modalidad)->first();

		$array_participantes = $modalidad->participantes;

		foreach ($array_participantes as $participante) {
			$participante->puntos = rand(0,100);

			try {
				$participante->save();
				return redirect('modalidades.mostrar')->with('mensaje', "Creado con éxito");
			} catch (Exception $ex){
				return redirect('modalidades.mostrar')->with('mensaje', "Fallo al editar los puntos");
			}
		}

	}
	public function postResetear($slug_modalidad){
		$modalidad = Modalidad::where('slug',$slug_modalidad)->first();
		$array_participantes = $modalidad->participantes;

		foreach ($array_participantes as $participante) {
			$participante->puntos = -1;
			try {
				$participante->save();
				return redirect('modalidades.mostrar');
			} catch (Exception $ex){
				return redirect('modalidades.mostrar');
			}
		}

	}

}
