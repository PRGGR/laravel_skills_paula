<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use SoapServer;
use App\Participante;
use App\Modalidad;
use Illuminate\Http\Request;
use App\PaulaLib\WSDLDocument;

class SoapServerController extends Controller
{
	private $clase = "\\App\\Http\\Controllers\\SkillsWebService";
	private $uri = "http://127.0.0.1/DWES/laravel_skills_paula/public/api";
	private $urlWSDL = "http://127.0.0.1/DWES/laravel_skills_paula/public/api/wsdl";
   
   	public function getServer(){
		$server = new SoapServer($this->urlWSDL);
		$server->setClass($this->clase);
		$server->handle(); 
		exit();
	}

	public function getWSDL(){
		$wsdl = new WSDLDocument($this->clase, $this->uri, $this->uri);
		$wsdl->formatOutput = true;
		header('Content-Type: text/xml');
		echo $wsdl->saveXML();
	}
}

class SkillsWebService{

	/**
	 * Método que devuelve un número de participantes de un centro pasado por parámetro
	 * @param string $centro 
	 * @return int
	 */
	public function getNumeroParticipantesCentro($centro){
		return Participante::where('centro',$centro)->count();
	}

	/**
	 * Método que devuelve un array de participantes de un tutor pasado por parámetro
	 * @param string $tutor 
	 * @return App\Participante[]
	 */
	public function getParticipantesTutor($tutor){
		return Participante::where('tutor', $tutor)->orderBy('puntos','desc')->get();
	}
}