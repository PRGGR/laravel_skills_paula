<?php

namespace App\Http\Controllers;

use App\Participante;
use App\Modalidad;
use Illuminate\Http\Request;

class ParticipantesController extends Controller
{
    public function getCrear(){
    	$modalidades = Modalidad::all();
		return view('participantes.crear', 
	    	array('arrayModalidades' => $modalidades));
	}

	public function postCrear(Request $request){
		
		$participante = new Participante();

		$participante->nombre = $request->nombre;
		$participante->apellidos = $request->apellidos;
		$participante->centro = $request->centro;
		$participante->tutor = $request->tutor;
		$participante->fechaNacimiento = $request->fechaNacimiento;
		$participante->modalidad_id = $request->modalidad;
		$participante->imagen = $request->imagen->store('','participantes');

		try {
			$participante->save();
			return redirect('modalidades')->with('mensaje', "Creado con éxito");
		} catch (Exception $ex){
			return redirect('modalidades')->with('mensaje', "Fallo al registrar el participante");
		}
	}

	public function buscarTutor(Request $request){
		$b = $request->tutor;
		$participantes = Participante::select('tutor')->where('tutor','like','%'.$b.'%')->distinct()->pluck('tutor'); 
		return response()->json($participantes);
	}

	public function getParticipantesMayor($slug_modalidad){
		$modalidad = Modalidad::where('slug',$slug_modalidad)->first()->id;

		$participante = Participante::where('modalidad_id',$modalidad)->orderBy('puntos','desc')->first()->get();
		return response()->json($participante);
	}

	
}
