<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $nombre
 * @property string $apellidos
 * @property string $puntos
 * @property string $centro
 * @property string $tutor
 */
class Participante extends Model
{
    protected $table = 'participantes';
}
