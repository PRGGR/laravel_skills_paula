<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ModalidadesController@getInicio');

Route::get('modalidades', 'ModalidadesController@getModalidades');

Route::get('modalidades/mostrar/{slug}', 'ModalidadesController@getModalidad');

Route::post('modalidades/puntuar/{slug}', 'ModalidadesController@postPuntuar');
Route::post('modalidades/resetear/{slug}', 'ModalidadesController@postResetear');

Route::get('participantes/inscribir', 'ParticipantesController@getCrear');
Route::post('participantes/inscribir', 'ParticipantesController@postCrear');

Route::any('api', 'SoapServerController@getServer');
Route::any('api/wsdl', 'SoapServerController@getWSDL');

Route::get('rest/ganador/{slug}', 'ParticipantesController@getParticipantesMayor');

Route::post('busquedaAjax', 'ParticipantesController@buscarTutor');
