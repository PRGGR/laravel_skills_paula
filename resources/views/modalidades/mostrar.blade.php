@extends('layouts.master')
@section('titulo')
	Mostrar
@endsection
@section('contenido')
	<div class="row">		
		<div class="col-xs-12 col-sm-6">
				<h1>{{$modalidad->nombre}}</h1>
				<h5>Familia profesional: {{$modalidad->familiaProfesional}}</h5>
				<h5>Participantes</h5>
				@php
				 $array_participantes = $modalidad->participantes;
				@endphp
				@foreach ($array_participantes as $participante)
					<div class="cuadro">
						<div style="margin-left: 50px">
							<p>{{$participante->nombre}}</p><br>
							<img src="{{asset('assets/imagenes/participantes')}}/{{$participante->imagen}}" style="height:200px"/>
						</div>
					</div>
				
				<br>
				@endforeach

				<form action="" method="post" accept-charset="utf-8">
					<a class="btn btn-danger" href="{{ url('/modalidades/puntuar/' . $modalidad->slug ) }}">Puntuar</a>
					<a class="btn btn-danger" href="{{ url('/modalidades/resetear/' . $modalidad->slug ) }}">Resetear</a>
				</form>


				<div {{-- style="display:none" --}}>
					
					<h2>Resultados</h2>
					<table class="table table-striped">
						<tr>
							<th>Nombre</th>
							<th>Puntos</th>
						</tr>
						@foreach($array_participantes as $participante)
						<tr>
							<td>{{$participante->nombre}}</td>
							<td>{{$participante->puntos}}</td>
						</tr>
						@endforeach
					</table>

				</div>
			</div>
		</div>
@endsection