@extends('layouts.master')
@section('titulo')
	Index
@endsection
@section('contenido')
	<div class="row">
		<table class="table table-striped">
			<tr>
			@foreach($arrayModalidades as $modalidad)
				<td>
					<h3><a href="{{ url('/modalidades/mostrar/' . $modalidad->slug ) }}">{{$modalidad->nombre}}</a></h3>
					<p>{{$modalidad->participantes->count()}} participantes</p>
					<img src="{{asset('assets/imagenes/modalidades')}}/{{$modalidad->imagen}}" style="height:200px; float:left;"/>
				</td>
			</tr>

			@endforeach
		</table>
	</div>
@endsection