@extends('layouts.master')
@section('titulo')
	Crear
@endsection
@section('contenido')
	<div class="row">
		<div class="offset-md-3 col-md-6">
			<div class="card">
				<div class="card-header text-center">
				 Añadir nuevo participante
				</div>
				<div class="card-body" style="padding:30px">
					<form action="{{ action('ParticipantesController@postCrear') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}

						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" name="nombre" id="nombre" class="form-control">
						</div>
						<div class="form-group">
							<label for="apellidos">Apellidos</label>
							<input type="text" name="apellidos" id="apellidos" class="form-control">
						</div>
						<div class="form-group">
							<label for="centro">Centro</label>
							<input type="text" name="centro" id="centro" class="form-control">
						</div>
						<div class="form-group">
							<label for="tutor">Tutor</label>
							<input type="text" name="tutor" id="tutor" class="form-control">
						</div>
					    <script>
					      $(document).ready(function(){
					        $('#tutor').autocomplete({
					          source : function( request, result){
					            $.ajax({
					              type : 'POST',
					              url : '{{url('busquedaAjax')}}',
					              dataType :'json',
					              data : { '_token': '{{ csrf_token() }}','tutor' : request['term'] },
					              success : function(data){ // en datos, es lo que recibimos de validar.php
					                result(data);
					              },          
					              error : function(xhr, status){
					                alert("Error en el proceso de validación JSON");            
					              }
					            });
					          }
					        });
					      });
					    </script>
						<div class="form-group">
							<label for="fechaNacimiento">Fecha de nacimiento</label>
							<input type="date" name="fechaNacimiento" id="fechaNacimiento" class="form-control">
						</div>
						<div class="form-group">
							<label for="modalidad">Modalidad</label>
								<select name="modalidad" id="modalidad">
								@foreach($arrayModalidades as $modalidad)
										<option id="{{$modalidad->id}}" name="{{$modalidad->id}}" value="{{$modalidad->id}}">{{$modalidad->nombre}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="imagen">Foto</label>
							<input type="file" name="imagen" id="imagen" class="form-control">
						</div>
						<div class="form-group text-center">
							<input type="submit" class="btn btn-danger" style="padding:8px 100px;margin-top:25px;" value="Añadir participante">
						</div>
					</form>
				</div>
			</div>
		 </div>
	</div>
@endsection